#!/usr/bin/env python3

import argparse
import logging
import boto3
import botocore
from botocore.exceptions import ClientError
from enum import Enum, unique
import os.path
from os import path
from pathlib import Path
import base64
import sys
import hashlib
import shutil

# Remove
import pprint


@unique
class Host(Enum):
    AWS_S3 = "s3"
    LOCAL = "local"


@unique
class Location(Enum):
    SOURCE = "source"
    DESTINATION = "destination"


source_arg = None
destination_arg = None
delete_source_on_success_arg = False


def check_bucket(bucket_name: str, s3_client: object):
    try:
        s3_client.head_bucket(Bucket=bucket_name)
        return True
    except botocore.exceptions.ClientError as e:
        # If a client error is thrown, then check that it was a 404 error.
        # If it was a 404 error, then the bucket does not exist.
        error_code = int(e.response['Error']['Code'])
        if error_code == 403:
            print("Private bucket. Forbidden access!")
            return True
        elif error_code == 404:
            print(f"Bucket {bucket_name} does not exist!")
            return False


def build_object(_path: str, _location: str, s3_resource: object,
                 s3_client: object, source_path_object: dict,
                 destination_path_object: dict):
    if _path.startswith("s3://"):
        bucket_name = _path.replace('s3://', '')
        bucket = s3_resource.Bucket(bucket_name)
        if check_bucket(bucket_name, s3_client):
            if _location == Location.SOURCE:
                source_path_object["host"] = Host.AWS_S3
                for obj in bucket.objects.all():
                    source_path_object["files"].append({
                        'filename':
                        obj.key,
                        'md5_bin':
                        calculate_hash(obj, Host.AWS_S3)
                    })
            else:
                destination_path_object["host"] = Host.AWS_S3
        else:
            exit(1)
    elif path.exists(_path):
        if _location == Location.SOURCE:
            source_path_object["host"] = Host.LOCAL
            files = list()
            for (root, _, filenames) in os.walk(_path):
                files += [os.path.join(root, file) for file in filenames]
            filenames_sorted = sorted(files, key=str, reverse=True)
            for f in filenames_sorted:
                md5_hash = calculate_hash(f, Host.LOCAL)
                source_path_object["files"].append({
                    'filename': f,
                    'md5_bin': md5_hash
                })
        else:
            destination_path_object["host"] = Host.LOCAL
    else:
        print(f'[ERROR] Error parsing path {_path}.')
        exit(1)


def calculate_hash(file, host):
    ret = None
    if host == Host.LOCAL:
        md5_hash = hashlib.md5()
        a_file = open(file, "rb")
        content = a_file.read()
        md5_hash.update(content)
        ret = md5_hash.hexdigest()
    else:
        ret = file.e_tag[1:-1]
    return ret


def delete(
    file,
    host,
    s3_location=None,
    s3_client=None,
):
    ret = False
    if host == Host.LOCAL:
        os.remove(file)
        # TODO: fix remove empty dirs
        dirname = os.path.dirname(file)
        empty_dirs = []
        root_dir = dirname.split("/")[0]
        # Iterate over the directory tree and check if directory is empty.
        for (root, dirnames, filenames) in os.walk(root_dir):
            if len(dirnames) == 0 and len(filenames) == 0:
                empty_dirs.append(root)
        [shutil.rmtree(d) for d in empty_dirs if d != root_dir]
    else:
        s3_client.delete_object(Bucket=s3_location, Key=file)
    return ret


def copy(source_metafile,
         source_host,
         destination,
         destination_host,
         s3_source_location=None,
         s3_destination_location=None,
         s3_client=None,
         s3_resource=None,
         delete_source_on_success=False):

    destination_metafile = dict()

    sfile = source_metafile['filename']

    # From local to local [OK]
    if (source_host is Host.LOCAL) and (destination_host is Host.LOCAL):
        destination_dir = destination + (os.path.dirname(sfile).replace(
            os.path.dirname(sfile).split('/')[0], ''))
        Path(destination_dir).mkdir(parents=True, exist_ok=True)
        shutil.copy2(sfile, destination_dir)

        # Fill file's data
        destination_metafile['filename'] = sfile
        destination_metafile['md5_bin'] = calculate_hash(
            sfile.replace(os.path.dirname(sfile), destination_dir), Host.LOCAL)

        if delete_source_on_success:
            if source_metafile["md5_bin"] == destination_metafile["md5_bin"]:
                delete(source_metafile["filename"], Host.LOCAL)
            else:
                print(
                    f'[WARN] Source file [{source_metafile["filename"]}] and Destination file [{destination_metafile["filename"]}] are differents. Source file won\'t be deleted.'
                )

    # From local to aws s3 [OK]
    elif (source_host is Host.LOCAL) and (destination_host is Host.AWS_S3):
        d = destination.replace('s3://', '')
        destination_file = sfile.replace(s3_source_location + "/", '')
        s3_client.upload_file(sfile, d, destination_file)
        obj = s3_resource.Object(d, destination_file)

        # Fill file's data
        destination_metafile['filename'] = sfile
        destination_metafile['md5_bin'] = calculate_hash(obj, Host.AWS_S3)

        if delete_source_on_success:
            if source_metafile["md5_bin"] == destination_metafile["md5_bin"]:
                delete(source_metafile["filename"], Host.LOCAL, s3_client)
            else:
                print(
                    f'[WARN] Source file [{source_metafile["filename"]}] and Destination file [{destination_metafile["filename"]}] are diferents. Source file won\'t be deleted.'
                )

    # From aws s3 to local [OK]
    elif (source_host is Host.AWS_S3) and (destination_host is Host.LOCAL):
        s = s3_source_location.replace('s3://', '')
        dirname = os.path.dirname(sfile)
        if dirname != '':
            os.makedirs(destination + "/" + dirname, exist_ok=True)
        file = destination + "/" + sfile
        s3_client.download_file(s, sfile, file)

        # Fill file's data
        destination_metafile['filename'] = sfile
        destination_metafile['md5_bin'] = calculate_hash(file, Host.LOCAL)

        if delete_source_on_success:
            if source_metafile["md5_bin"] == destination_metafile["md5_bin"]:
                delete(source_metafile["filename"], Host.AWS_S3, s, s3_client)
        else:
            print(
                f'[WARN] Source file [{source_metafile["filename"]}] and Destination file [{destination_metafile["filename"]}] are diferents. Source file won\'t be deleted.'
            )

    # From aws s3 to aws s3 [OK]
    elif (source_host is Host.AWS_S3) and (destination_host is Host.AWS_S3):
        s = s3_source_location.replace('s3://', '')
        d = s3_destination_location.replace('s3://', '')
        copy_source = {'Bucket': s, 'Key': sfile}

        s3_client.copy(copy_source, d, sfile)

        # Fill file's data
        destination_metafile['filename'] = sfile
        destination_metafile['md5_bin'] = calculate_hash(
            s3_resource.Object(s, sfile), Host.AWS_S3)

        if delete_source_on_success:
            if source_metafile["md5_bin"] == destination_metafile["md5_bin"]:
                delete(source_metafile["filename"], Host.AWS_S3, s, s3_client)
        else:
            print(
                f'[WARN] Source file [{source_metafile["filename"]}] and Destination file [{destination_metafile["filename"]}] are diferents. Source file won\'t be deleted.'
            )
    # Unknown combination
    else:
        print(f'[ERROR] Unknown source {sfile} and {destination}.')
        exit(1)

    return destination_metafile


def main(source: str, destination: str, delete_source_on_success=False):
    source_path_object = {
        "path": str,
        "host": str,
        "files": [],
    }

    destination_path_object = {
        "path": str,
        "host": str,
        "files": [],
    }

    source_path_object["path"] = source
    destination_path_object["path"] = destination

    s3_resource = boto3.resource('s3')
    s3_client = boto3.client('s3')

    build_object(args.source, Location.SOURCE, s3_resource, s3_client,
                 source_path_object, destination_path_object)

    build_object(args.destination, Location.DESTINATION, s3_resource,
                 s3_client, source_path_object, destination_path_object)

    for file in source_path_object["files"]:
        ret = copy(source_metafile=file,
                   source_host=source_path_object["host"],
                   destination=destination_path_object["path"],
                   destination_host=destination_path_object["host"],
                   s3_client=s3_client,
                   s3_resource=s3_resource,
                   s3_source_location=source_path_object["path"],
                   s3_destination_location=destination_path_object["path"],
                   delete_source_on_success=delete_source_on_success)
        destination_path_object["files"].append(ret)

    # pprint.pprint(f"source_path_object: {source_path_object}")
    # pprint.pprint(f"destination_path_object: {destination_path_object}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-s",
                        "-o",
                        "--source",
                        "--origem",
                        dest='source',
                        type=str,
                        help="source path.",
                        required=True)

    parser.add_argument("-d",
                        "--destination",
                        "-t",
                        "--target",
                        dest='destination',
                        type=str,
                        help="destination path.",
                        required=True)

    parser.add_argument("--delete",
                        dest="delete_source_on_success",
                        help="delete source files (default: False)",
                        required=False,
                        action='store_true')  # Argument without value

    args = parser.parse_args()

    main(source=args.source,
         destination=args.destination,
         delete_source_on_success=args.delete_source_on_success)
