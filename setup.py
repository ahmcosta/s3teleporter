import setuptools

setuptools.setup()

# from setuptools import setup, find_packages

# import sys

# setup(
#     name="s3-teleporter",
#     version="0.0.1",
#     description="Teleport tool to allow copy from/to/between AWS S3",
#     author="Antonio Henrique de Melo Costa",
#     author_email="ahmcosta@gmail.com",
#     url='https://gitlab.com/ahmcosta/s3-teleporter',
#     packages=find_packages(),
#     provides=["s3-teleporter"],
#     license="GNU Lesser General Public License v3 or later",
#     # install_requires=['awscli', 'boto'],
#     classifiers=[
#         "Programming Language :: Python",
#         "Programming Language :: Python :: 3",
#         "Programming Language :: Python :: 2",
#         "Development Status :: 1 - Beta",
#         "Intended Audience :: Developers",
#         "Operating System :: OS Independent",
#         "License :: OSI Approved :: GNU Lesser General Public License v3 or later (LGPLv3+)",
#         "Topic :: Software Development :: Libraries :: Python Modules",
#     ],
# )
