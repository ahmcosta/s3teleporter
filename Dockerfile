FROM ubuntu:20.04 AS builder

ENV TZ=America/Sao_Paulo
ENV DEBIAN_FRONTEND=noninteractive

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

ADD . /s3teleporter
WORKDIR /s3teleporter

# https://dh-virtualenv.readthedocs.io/en/latest/tutorial.html#step-1-install-dh-virtualenv
# https://launchpad.net/~jyrki-pulliainen/+archive/ubuntu/dh-virtualenv
RUN echo "deb [trusted=yes] http://ppa.launchpad.net/jyrki-pulliainen/dh-virtualenv/ubuntu focal main" \
	> /etc/apt/sources.list.d/jyrki-pulliainen-ubuntu-dh-virtualenv-focal.list

RUN cat /etc/apt/sources.list.d/jyrki-pulliainen-ubuntu-dh-virtualenv-focal.list

RUN apt-get update && \
	apt install \
	python3 \
	python3-pip \
	python3-venv \
	debhelper \
	dh-python \
	dh-virtualenv \
	devscripts \
	build-essential \
	lintian \
	git \
	-y || true

RUN python3 -m pip install virtualenv build

RUN debuild -us -uc -b

