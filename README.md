# Description
Teleport tool to allow copy from/to/between Amazon S3

# Usage
```bash
python3 s3teleporter.py --help
usage: s3teleporter.py [-h] -s SOURCE -d DESTINATION [--delete]

optional arguments:
  -h, --help            show this help message and exit
  -s SOURCE, -o SOURCE, --source SOURCE, --origem SOURCE
                        source path.
  -d DESTINATION, --destination DESTINATION, -t DESTINATION, --target DESTINATION
                        destination path.
  --delete              delete source files (default: False)
```

## Install on Ubuntu 16.04 (Xenial Xerus)

s3teleporter needs python >= 3.8. So you should install [deadsnakes](https://launchpad.net/~deadsnakes/+archive/ubuntu/ppa) repo.

```bash
echo "deb [trusted=yes] http://ppa.launchpad.net/deadsnakes/ppa/ubuntu xenial main" > /etc/apt/sources.list.d/deadsnakes-xenial.list

apt update -qq

apt install python3.8
```

After that, configure python3.8 to be default python3 command.

```bash
update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.8 1

update-alternatives --config python3
```

Now you can install s3teleporter package like that

```bash
apt install ./s3teleporter_1.0.0_amd64.deb
```